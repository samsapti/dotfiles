# Enable p10k instant prompt
[[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]] &&
    source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"

# Load the completion system
autoload -Uz compinit && compinit

# Source *.zsh files
for zfile in $ZDOTDIR/*.zsh; do
    source $zfile
done

# Source NVM
[[ -f /usr/share/nvm/init-nvm.sh ]] && source /usr/share/nvm/init-nvm.sh

# Set variables for antidote
antidote_dir="$ZDOTDIR/.antidote"
plugins_txt="$ZDOTDIR/.zsh_plugins.txt"
static_file="$ZDOTDIR/.zsh_plugins.zsh"

# Clone antidote if necessary and generate a static plugin file
if [[ ! $static_file -nt $plugins_txt ]]; then
    [[ -e $antidote_dir ]] ||
        git clone --depth=1 https://github.com/mattmc3/antidote.git $antidote_dir
    (
        source $antidote_dir/antidote.zsh
        antidote bundle <$plugins_txt >$static_file
    )
fi

# Make antidote commands available
autoload -Uz $antidote_dir/functions/antidote

# Source the static plugins file
source $static_file

# Cleanup
unset antidote_dir plugins_file static_file

# Set p10k prompt
autoload -Uz promptinit && promptinit && prompt powerlevel10k

# Load p10k configuration
[[ -f $ZDOTDIR/.p10k.zsh ]] && source $ZDOTDIR/.p10k.zsh
