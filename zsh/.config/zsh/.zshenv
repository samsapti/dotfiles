# PATH
export PATH="$HOME/.local/bin:$PATH"
(( ${+commands[go]} )) && export PATH="$(go env GOPATH)/bin:$PATH"

# Language
export LANG="en_US.UTF-8"

# Essentials
export PAGER="less"
export TERM="xterm-256color"
(( ${+commands[nano]} )) && export EDITOR="nano"
(( ${+commands[vim]} )) && export EDITOR="vim"
(( ${+commands[nvim]} )) && export EDITOR="nvim"

# Less
export LESS="-R"
export LESSHISTFILE="/dev/null"

# Lol
export ANSIBLE_NOCOWS=1

# Secret variables
[[ -f $ZDOTDIR/.env.zsh ]] && source $ZDOTDIR/.env.zsh
