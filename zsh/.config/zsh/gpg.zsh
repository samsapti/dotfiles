if [[ -z $SSH_CONNECTION ]] && [[ $HOSTNAME != "toolbox" ]]; then
    export GPG_TTY="$TTY"
    gpgconf --launch gpg-agent
    gpg-connect-agent updatestartuptty /bye &> /dev/null
fi
