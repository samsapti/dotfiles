bt() {
    (( ${+commands[bluetoothctl]} )) || return 2

    if [[ $1 == "on" ]]; then
        (( ${+commands[connmanctl]} )) &&
            connmanctl enable bluetooth && sleep 0.5
        bluetoothctl power on
    elif [[ $1 == "off" ]]; then
        bluetoothctl power off
        (( ${+commands[connmanctl]} )) &&
            connmanctl disable bluetooth
    else
        bluetoothctl "$@"
    fi
}

get_ip() { 
    (( ${+commands[curl]} )) || return 2
    local ARGS=()

    while [[ $# -gt 0 ]]; do
        [[ $1 == --ip ]] && local IP="?ip=$2" && shift || ARGS+=("$1")
        shift
    done

    (( ${+commands[jq]} )) && local JQ="| jq"
    eval "curl ${ARGS[@]} -fsSL 'https://ifconfig.co/json$IP' $JQ"
}

gpg_ssh() {
    if [[ $1 == "on" ]]; then
        export SSH_AUTH_SOCK_OLD="$SSH_AUTH_SOCK"
        export SSH_AUTH_SOCK="$(gpgconf --list-dirs agent-ssh-socket)"
    elif [[ $1 == "off" ]]; then
        [[ -n $SSH_AUTH_SOCK_OLD ]] && export SSH_AUTH_SOCK="$SSH_AUTH_SOCK_OLD"
        unset SSH_AUTH_SOCK_OLD
    fi
}

http() {
    (( ${+commands[python]} )) || return 2
    [[ $# -gt 0 ]] && local DIR="--directory=$1" && shift
    eval "python -m http.server 8080 --bind 127.0.0.1 $DIR $@"
}

open() {
    if [[ $# -gt 1 ]] && (( ${+commands[$1]} )); then
        nohup "$@" &> /dev/null & disown
    else
        (( ${+commands[xdg-open]} )) || return 2
        nohup xdg-open "$@" &> /dev/null & disown
    fi
}

wg() {
    (( ${+commands[wg]} )) && (( ${+commands[wg-quick]} )) || return 2

    if [[ $1 == "up" ]]; then
        sudo wg-quick up $2
    elif [[ $1 == "down" ]]; then
        sudo wg-quick down $2
    else
        command wg "$@"
    fi
}
