# Case insensitive completion for lowercase
zstyle ':completion:*' matcher-list 'm:{a-z}={A-Z}'

# Highlighted tab selection
zstyle ':completion:*' menu yes select

# Friendly names in Antidote
zstyle ':antidote:bundle' use-friendly-names 'yes'

# Extended globs
setopt extendedglob

# Right indentation off
ZLE_RPROMPT_INDENT=0
