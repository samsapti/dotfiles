if [[ $(. /etc/os-release && echo $ID) == "artix" ]] && [[ $(tty) == "/dev/tty1" ]]; then
    export XDG_CURRENT_DESKTOP="sway"
    export GTK_USE_PORTAL=1
    export QT_QPA_PLATFORM="wayland"
    
    dbus-launch --sh-syntax --exit-with-session sway &> /dev/null; exit 0
fi
