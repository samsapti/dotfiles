# File management
alias cp="cp -i"
alias mv="mv -i"
alias rm="rm -i"
alias rmf="rm -rf"
alias mkdir="mkdir -p"

# Navigation
alias -g ...="../.."
alias -g ....="../../.."
alias -g .....="../../../.."
alias -g ......="../../../../.."

# ls'ing
alias ls="ls --color=always"
(( ${+commands[eza]} )) && {
    alias eza="eza -hF --color=always --git --icons"
    alias la="eza -l"
    alias l="eza -al"
    alias ll="eza -aagl@"
    alias lag="la --git-ignore"
    alias lg="l --git-ignore"
    alias llg="ll --git-ignore"
    alias t="eza --tree -I '.git|node_modules'"
    alias tl="t -l"
    alias tla="tl -a"
    alias tlaa="tla -g@"
}

# Git
(( ${+commands[git]} )) && {
    alias gi="git init"
    alias gsa="git submodule add"
    alias gsur="git submodule update --remote"
}

# Vagrant
(( ${+commands[vagrant]} )) && {
    alias vagrant-scp="scp -F =(vagrant ssh-config) -i =(ssh-add -L)"
    alias vagrant-ssh="ssh -F =(vagrant ssh-config) -i =(ssh-add -L)"
}

# Filesystems
(( ${+commands[udisksctl]} )) && {
    alias lock="udisksctl lock -b"
    alias mount="udisksctl mount -b"
    alias unlock="udisksctl unlock -b"
    alias unmount="udisksctl unmount -b"
}

# Searching
alias grep="grep --color=always"
alias zgrep="zgrep --color=always"
(( ${+commands[rg]} )) &&
    alias rgh="rg --hidden -g '!/.git/'"

# Misc.
alias cl="clear"
alias e="\$EDITOR"
alias se="sudoedit"
alias q="exit 0"
alias visudo="sudo EDITOR=\"rvim -nc 'set nobackup nowritebackup'\" visudo"
(( ${+commands[tmux]} )) &&
    alias tmux="tmux -2u"
(( ${+commands[bat]} )) && {
    alias pbat="bat -pp"
    alias bless="bat --style=plain --paging=always"
    alias nbless="bat --paging=always"
}

# Platform specific
if [[ $(. /etc/os-release && echo $ID) == "artix" ]]; then
    alias pacin="sudo pacman -S"
    alias pacls="pacman -Qkl"
    alias pacown="pacman -Qo"
    alias pacqi="pacman -Qi"
    alias pacqs="pacman -Qs"
    alias pacrm="sudo pacman -Rcsu"
    alias pacsi="pacman -Si"
    alias pacss="pacman -Ss"
fi
