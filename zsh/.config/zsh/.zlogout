kill_services() {
    pkill "^mako$"
    pkill "^wireplumber$"
    pkill "^pipewire-pulse$"
    pkill "^pipewire$"
}

[[ $(. /etc/os-release && echo $ID) == "artix" ]] && [[ $(tty) == "/dev/tty1" ]] &&
    kill_services &> /dev/null
