# Zsh config

For plugins, this Zsh config uses
[Antidote](https://github.com/mattmc3/antidote.git), a minimal and easy
to use Zsh plugin manager.

Instead of directly using Oh-My-Zsh, we load specific plugins from it. I
like to keep it simple. Also, Oh-My-Zsh is slow, this is much faster.
