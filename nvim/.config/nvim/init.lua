-- plugins
require('packer').startup(function(use)
   use 'wbthomason/packer.nvim'
   use 'ii14/onedark.nvim'
   use {
      'mengelbrecht/lightline-bufferline',
      requires = {
         'itchyny/lightline.vim'
      }
   }
   use 'kyazdani42/nvim-web-devicons'
   use 'sheerun/vim-polyglot'
   use 'editorconfig/editorconfig-vim'
   use {
      'williamboman/mason-lspconfig.nvim',
      requires = {
         'williamboman/mason.nvim',
         'neovim/nvim-lspconfig'
      }
   }
   use {
      'hrsh7th/nvim-cmp',
      requires = {
         'hrsh7th/cmp-nvim-lsp',
         'saadparwaiz1/cmp_luasnip',
         'L3MON4D3/LuaSnip'
      }
   }
end)

-- basics
vim.opt.number = true
vim.opt.showmode = false
vim.opt.showtabline = 2
vim.opt.tabstop = 4
vim.opt.shiftwidth = 4
vim.opt.softtabstop = 0
vim.opt.expandtab = true
vim.opt.termguicolors = true
vim.opt.completeopt = { 'menu', 'menuone', 'noselect', 'noinsert' }
vim.cmd.syntax('on')
vim.cmd.colorscheme('onedark')

-- settings for specific filetypes
vim.api.nvim_create_autocmd('FileType', {
   pattern = { 'html', 'css', 'scss', 'json', 'yaml', 'yaml.*' },
   callback = function()
      vim.opt_local.tabstop = 2
      vim.opt_local.shiftwidth = 2
   end
})
vim.api.nvim_create_autocmd('FileType', {
   pattern = 'lua',
   callback = function()
      vim.opt_local.tabstop = 3
      vim.opt_local.shiftwidth = 3
   end
})
vim.api.nvim_create_autocmd({ 'BufNewFile', 'BufRead' }, {
   pattern = { 'zsh*', '.zsh*', '*.zsh' },
   callback = function()
      vim.opt.filetype = 'sh'
   end
})

-- appearance
vim.cmd.highlight { 'Normal', 'guibg=NONE', 'ctermbg=NONE' }
vim.g.background = 'dark'

-- lightline config
vim.g.lightline = {
   colorscheme = 'one',
   tabline = {
      left = { { 'buffers' } },
      right = { { 'relativepath' } }
   },
   component_expand = {
      buffers = 'lightline#bufferline#buffers'
   },
   component_type = {
      buffers = 'tabsel'
   }
}

-- lightline-bufferline config
vim.g['lightline#bufferline#show_number'] = 2
vim.g['lightline#bufferline#shorten_path'] = 0
vim.g['lightline#bufferline#smart_path'] = 1
vim.g['lightline#bufferline#enable_devicons'] = 1
vim.g['lightline#bufferline#unnamed'] = '[No Name]'
vim.g['lightline#bufferline#icon_position'] = 'right'

-- autocompletion config
local cmp = require('cmp')
local luasnip = require('luasnip')

luasnip.config.setup {}

cmp.setup {
   snippet = {
      expand = function(args)
         luasnip.lsp_expand(args.body)
      end
   },
   mapping = {
      ['<C-d>'] = cmp.mapping.scroll_docs(-4),
      ['<C-f>'] = cmp.mapping.scroll_docs(4),
      ['<C-Space>'] = cmp.mapping.complete(),
      ['<C-e>'] = cmp.mapping.abort(),
      ['<CR>'] = cmp.mapping.confirm {
         behavior = cmp.ConfirmBehavior.Replace,
         select = true
      },
      ['<Tab>'] = cmp.mapping(function(fallback)
         if cmp.visible() then
            cmp.select_next_item()
         elseif luasnip.expand_or_jumpable() then
            luasnip.expand_or_jump()
         else
            fallback()
         end
      end, { 'i', 's' }),
      ['<S-Tab>'] = cmp.mapping(function(fallback)
         if cmp.visible() then
            cmp.select_prev_item()
         elseif luasnip.jumpable(-1) then
            luasnip.jump(-1)
         else
            fallback()
         end
      end, { 'i', 's' })
   },
   sources = {
      { name = 'nvim_lsp' },
      { name = 'luasnip' }
   }
}

cmp.setup.cmdline('/', {
   sources = {
      { name = 'buffer' }
   }
})

cmp.setup.cmdline(':', {
   sources = cmp.config.sources({
         { name = 'path' }
      },
      {
         { name = 'cmdline' }
      })
})

-- lsp config
require('mason').setup {}
require('mason-registry').refresh()
require('mason-lspconfig').setup {
   automatic_installation = true
}

local lsp = require('lspconfig')
local capabilities = require('cmp_nvim_lsp').default_capabilities()

lsp.ansiblels.setup {
   capabilities = capabilities
}
lsp.bashls.setup {
   capabilities = capabilities
}
lsp.clangd.setup {
   capabilities = capabilities
}
lsp.dockerls.setup {
   capabilities = capabilities
}
lsp.gopls.setup {
   capabilities = capabilities
}
lsp.lua_ls.setup {
   capabilities = capabilities,
   settings = {
      Lua = {
         runtime = {
            version = 'LuaJIT'
         },
         diagnostics = {
            globals = { 'vim' }
         },
         telemetry = {
            enable = false
         }
      }
   }
}
lsp.pylsp.setup {
   capabilities = capabilities
}
lsp.vimls.setup {
   capabilities = capabilities,
   init_options = {
      diagnostic = {
         enable = false
      },
      isNeovim = true
   }
}
lsp.yamlls.setup {
   capabilities = capabilities,
   settings = {
      redhat = {
         telemetry = {
            enabled = false
         }
      },
      yaml = {
         keyOrdering = false
      }
   }
}

-- keyboard mappings
vim.g.mapleader = ';'

-- buffer navigation
vim.keymap.set('n', '<Leader>1', '<Plug>lightline#bufferline#go(1)')
vim.keymap.set('n', '<Leader>2', '<Plug>lightline#bufferline#go(2)')
vim.keymap.set('n', '<Leader>3', '<Plug>lightline#bufferline#go(3)')
vim.keymap.set('n', '<Leader>4', '<Plug>lightline#bufferline#go(4)')
vim.keymap.set('n', '<Leader>5', '<Plug>lightline#bufferline#go(5)')
vim.keymap.set('n', '<Leader>6', '<Plug>lightline#bufferline#go(6)')
vim.keymap.set('n', '<Leader>7', '<Plug>lightline#bufferline#go(7)')
vim.keymap.set('n', '<Leader>8', '<Plug>lightline#bufferline#go(8)')
vim.keymap.set('n', '<Leader>9', '<Plug>lightline#bufferline#go(9)')
vim.keymap.set('n', '<Leader>0', '<Plug>lightline#bufferline#go(10)')

-- buffer deletion
vim.keymap.set('n', '<Leader>d1', '<Plug>lightline#bufferline#delete(1)')
vim.keymap.set('n', '<Leader>d2', '<Plug>lightline#bufferline#delete(2)')
vim.keymap.set('n', '<Leader>d3', '<Plug>lightline#bufferline#delete(3)')
vim.keymap.set('n', '<Leader>d4', '<Plug>lightline#bufferline#delete(4)')
vim.keymap.set('n', '<Leader>d5', '<Plug>lightline#bufferline#delete(5)')
vim.keymap.set('n', '<Leader>d6', '<Plug>lightline#bufferline#delete(6)')
vim.keymap.set('n', '<Leader>d7', '<Plug>lightline#bufferline#delete(7)')
vim.keymap.set('n', '<Leader>d8', '<Plug>lightline#bufferline#delete(8)')
vim.keymap.set('n', '<Leader>d9', '<Plug>lightline#bufferline#delete(9)')
vim.keymap.set('n', '<Leader>d0', '<Plug>lightline#bufferline#delete(10)')

-- navigating splits
vim.keymap.set('n', '<C-J>', '<C-W><C-J>')
vim.keymap.set('n', '<C-K>', '<C-W><C-K>')
vim.keymap.set('n', '<C-L>', '<C-W><C-L>')
vim.keymap.set('n', '<C-H>', '<C-W><C-H>')
