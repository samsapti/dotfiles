# Neovim config

This Neovim config requires [packer](https://github.com/wbthomason/packer.nvim) for plugins.

To install the plugins from the config file, first install packer, and then run `PackerSync` inside Neovim.
