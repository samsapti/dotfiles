#!/usr/bin/env dash

set -e

FILE="$1"
CLEAN="${FILE%.pdf}.clean.pdf"

pdftk "$FILE" dump_data_utf8 |
    sed -E 's/^InfoValue:.*$/InfoValue:/g' |
    pdftk "$FILE" update_info - output "$CLEAN"

exiftool -q -overwrite_original -all:all= "$CLEAN"
qpdf --linearize --replace-input "$CLEAN"

echo "$CLEAN"
