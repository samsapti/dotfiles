#!/usr/bin/env dash

[ "$(. /etc/os-release && echo $ID)" = "artix" ] &&
    { paru -Syu; echo; }

command -v flatpak >/dev/null 2>&1 &&
    { flatpak update; echo; }
command -v npm >/dev/null 2>&1 &&
    { npm update --location="global"; echo; }
command -v rustup >/dev/null 2>&1 &&
    { rustup update; echo; }
command -v docker >/dev/null 2>&1 && [ -n "$(sudo docker images -q 2>/dev/null)" ] &&
    { sudo sh -c "docker images |
        awk '(NR>1) && (\$2!~/none/) {print \$1\":\"\$2}' |
        xargs -l docker pull"; echo; }

zsh -ic "antidote update; echo"

echo "Updating dotfiles..."
git -C "$(dirname "$(readlink -e "$0")")" pull
