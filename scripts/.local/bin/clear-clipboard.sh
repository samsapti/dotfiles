#!/usr/bin/env dash

# Wayland clipboard
wl-copy -c
wl-copy -pc

# X clipboard
echo -n "" | xclip -selection clipboard -i
echo -n "" | xclip -selection primary -i
