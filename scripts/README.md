# Scripts

For these custom scripts to work, you need to add `$HOME/.local/bin` to your `PATH`. If you use my Zsh config, this is already taken care of.

My scripts use the `dash` shell as the interpreter (the shebang line), so install that to use them. Otherwise, change `#!/usr/bin/env dash` to `#!/usr/bin/env bash` or anything else that suits you.
