# My dotfiles

This is a collection of some of my dotfiles for my Linux systems. They
work on Artix and Fedora.

To install:

```sh
git clone https://git.data.coop/samsapti/dotfiles.git
cd dotfiles
./install.sh
```

### Notes
- You need to make `sudo` rules that allow you to execute,
  `poweroff` and `reboot` without a password, otherwise the
  keybindings and scripts for those won't work.
- The installation script requires GNU Stow to work.
- Make sure to thoroughly examine these dotfiles and change them to your
  needs before using them.
- See the READMEs in the individual subfolders here for special notes.
- I use `dash` instead of `sh` or `bash` for my shell scripts. As such,
  all my scripts try to be as POSIX compliant as possible. If you don't
  want to use `dash`, you can change the shebang in the scripts to
  `#!/bin/sh` or `#!/bin/bash`.

## License

The [Unlicense](https://git.data.coop/samsapti/dotfiles/src/branch/main/LICENSE).
