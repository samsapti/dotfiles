# Waybar config

This is a beautifully styled config for Waybar, featuring a solarized dark coloscheme and a custom module for showing IVPN connection status (thus requires IVPN client).

Note that the included IVPN scripts use the `dash` shell as the interpreter (the shebang line), so install that to use them. Otherwise, change `#!/usr/bin/env dash` to `#!/usr/bin/env bash` or anything else that suits you.
