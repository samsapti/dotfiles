# Sway config

My Sway config currently uses and/or depends on the following packages:

- `artix-backgrounds` for the wallpaper
- `grimshot` for screenshotting
- `sway-launcher-desktop` for launching applications
