#!/bin/sh

cd -P -- "$(readlink -e "$(dirname "$0")")" || exit 255
echo "=> Checking dependencies..."

command -v stow >/dev/null 2>&1 || { echo "Please install GNU Stow!"; exit 1; }

sleep 0.5
echo "=> Determining OS..."

if [ "$(. /etc/os-release && echo $ID)" = "artix" ]; then
    set -- "alacritty" "git" "mako" "nvim" "scripts" "sway" "swaylock" "swaynag" "waybar" "zsh"
    INSTALL="sudo pacman -S"
else
    echo "OS not supported!"
    exit 1
fi

sleep 0.5
echo "=> Installing dotfile packages..."

for pkg in "$@"; do
    sleep 0.1
    echo "  -> Stowing $pkg"
    
    # Symlink only the individual files instead of the entire directory
    [ "$pkg" = "nvim"    ] && NVIM=1 && mkdir -p "$HOME/.config/$pkg"
    [ "$pkg" = "scripts" ] && DASH=1 && mkdir -p "$HOME/.local/bin"
    [ "$pkg" = "zsh"     ] && ZSH=1  && mkdir -p "$HOME/.config/$pkg"

    LC_ALL="C" stow -t "$HOME" --ignore="README.md" "$pkg"
done

if [ -n "$NVIM" ] && [ ! -d "${XDG_DATA_HOME:-$HOME/.local/share}/nvim/site/pack/packer/start/packer.nvim" ]; then
    command -v nvim >/dev/null 2>&1 ||
        { sleep 0.5;
          echo "=> Installing Neovim";
          $INSTALL nvim; }

    sleep 0.5
    echo "=> Installing packer for Neovim"
    git clone --quiet --depth 1 "https://github.com/wbthomason/packer.nvim" \
        "${XDG_DATA_HOME:-$HOME/.local/share}/nvim/site/pack/packer/start/packer.nvim"
    echo "  :: The LSP servers will be installed automatically when you open a file in Neovim."
    echo "  :: Please run the following Normal mode command in Neovim to install the plugins:"
    echo "    :PackerSync"
fi

if [ -n "$DASH" ] && ! command -v dash >/dev/null 2>&1; then
    sleep 0.5
    echo "=> Installing Dash"
    $INSTALL dash
fi

if [ -n "$ZSH" ]; then
    if ! grep -q "$(id -nu).*/zsh$" /etc/passwd; then
        command -v zsh >/dev/null 2>&1 ||
            { sleep 0.5;
              echo "=> Installing Zsh";
              $INSTALL zsh; }

        sleep 0.5
        chsh -s /bin/zsh
    fi

    sleep 0.5
    echo "=> Initializing Zsh"
    echo "  :: Please run the following command from Zsh:"
    echo "    $ fast-theme ~/.config/zsh/fast-theme.ini"
fi

sleep 0.5
echo "=> Done!"
